import { useParams } from "react-router-dom";
import { useState, useEffect } from "react";
import { Posts } from "../../../../../server/db/entity/Posts";
import { Link } from "react-router-dom";
import styles from "../Main/main.module.scss";
import { POSTS } from "../../api/url";

export function PostPage() {
  const [post, setPost] = useState<Posts[] | undefined>();
  const [isAuthor, setIsAuthor] = useState(false);

  const { id } = useParams();

  async function getPost(id: number): Promise<void> {
    try {
      const res = await fetch(`${POSTS}/${id}`);
      if (!res.ok) {
        throw new Error("Server error");
      }
      const data = await res.json();
      setPost(data);
    } catch (error) {
      console.log(error);
    }
  }

  useEffect(() => {
    if (id) {
      getPost(Number(id));
    }
  }, [id]);

  useEffect(() => {
    const jwtToken = localStorage.getItem("token"); // отриманий JWT

    if (jwtToken && post && post[0].author.email) {
      const parts = jwtToken.split(".");
      const encodedPayload = parts[1];

      // Декодувати корисну навантаження Base64
      const decodedPayload = atob(encodedPayload);

      const payloadObj = JSON.parse(decodedPayload);

      if (payloadObj.email === post[0].author.email) {
        setIsAuthor(true);
      } else {
        setIsAuthor(false);
      }
    }
  }, [post]);

  return (
    <>
      {isAuthor && (
        <Link to={`/edit-post/${id}`}>
          <button
            className={`${styles["buttons"]} ${styles["buttons__change-post"]}`}
          >
            Змінити пост
          </button>
        </Link>
      )}

      <Link to={`/`}>
        <button
          className={`${styles["buttons"]} ${styles["buttons__back-to-all"]}`}
        >
          Назад до усіх новин
        </button>
      </Link>
      <div>
        {post && (
          <>
            <h3>{post[0].title}</h3>
            <p>{post[0].text}</p>
            <p>{post[0].genre}</p>
            <p>isPrivate: {post[0].isPrivate.toString()}</p>
            <p>email: {post[0].author.email}</p>
          </>
        )}
      </div>
    </>
  );
}

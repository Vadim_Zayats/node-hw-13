import "reflect-metadata";
import { DataSource } from "typeorm";
import { Users } from "./entity/User";
import { Posts } from "./entity/Posts";

export const AppDataSource = new DataSource({
  type: "mysql",
  host: "127.0.0.1",
  port: 3306,
  username: "root",
  password: "12345678",
  database: "homework-13",
  synchronize: true,
  logging: false,
  entities: [Users, Posts],
  migrations: [],
  subscribers: [],
});
